Este projeto ainda est� em fase de desenvolvimente e, por isso, possui funcionalidades que est�o incompletas ou que ainda n�o funcionam.

Para que este programa seja instalado no android, o usu�rio deve marcar a aceita��o de produtos n�o autorizados nas configura��es do Android.


-Cadastro e login de usu�rios ainda n�o funcionam. Para fazer login, deve ser utilizado facebook.

-A op��o de logoff ainda n�o funciona. Para trocar de usu�rio, deve-se limpar o banco de dados (Por meio de configura��es do Android).

-O programa para de funcionar quando "evento atual" � selecionado, mas nenhum evento foi selecionado (Quando um evento novo � criado, ele � selecionado atualmente).

-O banco de dados ainda n�o foi povoado. O usu�rio deve adicionar restaurantes e itens que ser�o compartilhados. Tamb�m � necess�rio fazer a adi��o de contatos manualmente.

-O compartilhamento de card�pios e restaurantes ainda n�o foi implementado. Quando ele for implementado, o banco de dados de card�pios e restaurantes ser� povoado pelos pr�prios usu�rios ou estabelecimentos. A principal utiliza��o do sistema ser� selecionar os itens e quem ir� compartilha-los.

-O design das interfaces n�o est� concluido. As interfaces atuais s�o apenas para ilustrar as utilidades b�sicas do sistema.

-� poss�vel que aconte�am v�rios outros bugs durante a execu��o do programa.