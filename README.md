Conta Limpa, versão 0.1

Este projeto ainda está em fase de desenvolvimento e, por isso, possui funcionalidades que estão incompletas ou que ainda não foram implementadas.

Para que este programa seja instalado no Android, o usuário deve marcar a aceitação de produtos não autorizados nas configurações do Android.


-Cadastro e login de usuários ainda não funcionam. Para fazer login, deve ser utilizado Facebook.

-A opção de log off ainda não funciona. Para trocar de usuário, deve-se limpar o banco de dados (Por meio de configurações do Android).

-O programa para de funcionar quando "evento atual" é selecionado, mas nenhum evento foi selecionado (Quando um evento novo é criado, ele é selecionado atualmente).

-O banco de dados ainda não foi povoado. O usuário deve adicionar restaurantes e itens que serão compartilhados. Também é necessário fazer a adição de contatos manualmente.

-O compartilhamento de cardápios e restaurantes ainda não foi implementado. Quando ele for implementado, o banco de dados de cardápios e restaurantes será povoado pelos próprios usuários ou estabelecimentos. A principal utilização do sistema será selecionar os itens e quem irá compartilha-los.

-O design das interfaces não está concluído. As interfaces atuais são apenas para ilustrar as utilidades básicas do sistema.

-É possível que aconteçam vários outros bugs durante a execução do programa.